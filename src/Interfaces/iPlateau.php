<?php

namespace App\Interfaces;


interface iPlateau
{
    function __construct($with,$height);
    public function getWidth();
    public function getHeight();
    public function isPositionValid( iPosition $position):bool;
}
