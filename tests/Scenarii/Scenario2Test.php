<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use \App\Model\Rover;
use \App\Model\Heading;
use \App\Model\Plateau;
use \App\Model\Position;
use \App\Interfaces\iInstruction;
use \App\Model\Instruction;

final class Scenario2Test extends TestCase
{

    public function testFull2(): void
    {
        $rover = new Rover(new Plateau(5, 5), new Position(3, 3), new Heading(Heading::HEADING_EAST));
        $rover->setDebug(false);

        $rover->action(new Instruction(iInstruction::ACTION_MOVE));
        $rover->action(new Instruction(iInstruction::ACTION_MOVE));
        $rover->action(new Instruction(iInstruction::ACTION_TURN_RIGHT));

        $rover->action(new Instruction(iInstruction::ACTION_MOVE));
        $rover->action(new Instruction(iInstruction::ACTION_MOVE));
        $rover->action(new Instruction(iInstruction::ACTION_TURN_RIGHT));

        $rover->action(new Instruction(iInstruction::ACTION_MOVE));
        $rover->action(new Instruction(iInstruction::ACTION_TURN_RIGHT));
        $rover->action(new Instruction(iInstruction::ACTION_TURN_RIGHT));
        $rover->action(new Instruction(iInstruction::ACTION_MOVE));

        $this->assertSame(5, $rover->getPosition()->getX());
        $this->assertSame(1, $rover->getPosition()->getY());
        $this->assertSame(Heading::HEADING_EAST, $rover->getHeading()->asString());
    }


}
